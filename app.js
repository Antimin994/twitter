class Card {
  constructor(id, title, body, name, email) {
    this.id = id;
    this.title = title;
    this.body = body;
    this.name = name;
    this.email = email;
    this.container = document.createElement('div');
    this.deleteBtn = document.createElement('button');
  }
  createElements() {
    this.container.className = "todo";
    this.container.innerHTML = `<h4>${this.title}</h4>
    <p class="body">${this.body}</p>
    <p class="name">Posted by ${this.name}, e-mail: ${this.email}</p>`
    this.deleteBtn.innerHTML = 'delete';
    this.container.append(this.deleteBtn);
    this.deleteBtn.addEventListener('click', () =>{
        fetch(`https://ajax.test-danit.com/api/json/posts/${this.id}`, {
          method: 'delete',
        })
        .then(({status}) => {
            if(status === 200) this.container.remove();
        });
    })
  }
  render(selector) {
    this.createElements();
    document.querySelector(selector).append(this.container);
  }


}


async function renderTwit() {
  const userResponse = await fetch('https://ajax.test-danit.com/api/json/users', {
    method: 'GET'
  })
  const usersData = await userResponse.json();
  const postResponse = await fetch('https://ajax.test-danit.com/api/json/posts', {
    method: 'GET'
  })
  const postsData = await postResponse.json();
  console.log(usersData);
  console.log(postsData);

  postsData.forEach((value, index, array) => {

    usersData.forEach((val, i, arr) => {
      if (val.id === value.userId) {
        toDoList = new Card(value.id, value.title, value.body, val.name, val.email);
        console.log(toDoList);
        toDoList.render('#root');
      }
    })
  })
}

window.onload = renderTwit();
